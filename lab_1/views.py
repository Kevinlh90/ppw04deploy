from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Muhamad Fariz Farhan' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 12, 8) #TODO Implement this, format (Year, Month, Date)
npm = 1706043380 # TODO Implement this

mhs_depan = 'Alya Zahra'
birth_depan = date(1999, 7, 22)
npm_depan = 1706039906
desc_depan = 'Seorang mahasiswa berumur 19 tahun yang mencoba survive dalam perkuliahan'
hobi_depan = 'Menggambar'

mhs_belakang = 'Annisaa Kayza Adisti'
birth_belakang = date(1999, 11, 16)
npm_belakang = 1706039906
desc_belakang = 'Seorang mahasiswa yang seru dan bersemangat'
hobi_belakang = 'Nonton Film/Drama, Mendengarkan Musik'


# Create your views here.
def index(request):
    response = {'name_saya': mhs_name, 'age_saya': calculate_age(birth_date.year), 'npm_saya': npm,
	'nama_depan': mhs_depan, 'age_depan': calculate_age(birth_depan.year), 'npm_depan':npm_depan, 'desc_depan':desc_depan, 'hobi_depan':hobi_depan,
	'nama_belakang': mhs_belakang, 'age_belakang': calculate_age(birth_belakang.year), 'npm_belakang':npm_belakang, 'desc_belakang':desc_belakang, 'hobi_belakang':hobi_belakang}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
